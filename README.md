# How to setup

1. Download the project to your local.
2. Go to the folder of the project and copy .env.example to .env . Also can run `cp .env.example .env` command.
3. In the newly copied/created .env, fill these:
    - DATABASE_NAME with the db name you have prepared
    - DATABASE_USERNAME and DATABASE_PASSWORD with your db credentials
    - ADMIN_JWT_SECRET with anything you want
4. Run `yarn`
    1. In case of something like
    `Error: Module not found: Can't resolve 'react-quill' in ./plugins/wysiwyg/admin/src/components/QuillEditor`, go to plugins/the_affected_module folder and run `yarn`
5. Run `yarn develop` for watch mode
6. Open the admin page. Usually it’s http://localhost:1337/admin
7. Fill in the data for the first user who will be the (super) admin. For standards, do use our glorious CEO. You could always use something else though.
    - Aubert Reginald
    - aubert@maven.co.id
    - Please ask Ignasius or other devs for standard password
