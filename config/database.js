module.exports = ({ env }) => ({
  defaultConnection: "default",
  connections: {
    default: {
      connector: "bookshelf",
      settings: {
        client: env("DATABASE_DRIVER", "mysql"),
        host: env("DATABASE_HOST", "127.0.0.1"),
        port: env.int("DATABASE_PORT", 3306),
        database: env("DATABASE_NAME", "your_database"),
        username: env("DATABASE_USERNAME", "your_user"),
        password: env("DATABASE_PASSWORD", "your_password"),
        ssl: env.bool("DATABASE_SSL", false),
      },
      options: {},
    },
  },
});
